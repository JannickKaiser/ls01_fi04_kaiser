/** Fehlersuche
 * @version 3.0 vom 01.12.2020
 * @author Tenbusch
 */  
import java.util.Scanner;

public class Aufgabe3 {
    
    public static void main(String[] args) {
    	umfangsberechnung();
    
    }
    public static String umfangsberechnung() {  //die Zeile ist korrekt, Finger weg!
    	
    	//Variablendeklaration
    	Scanner scan = new Scanner(System.in);
    	final double pi = 3.141;
    	double durchmesser;
    	double umfang;
    	String ueberschrift = "Umfangsberechnung eines eckigen Kreises";
        
    	//Eingabe
    	System.out.println(ueberschrift);
    	System.out.println("Bitte geben Sie den Durchmesser ein: ");
    	durchmesser = scan.nextDouble();
    	
    	//Verarbeitung
    	umfang = pi + pi * durchmesser;
        
    	//Ausgabe
    	System.out.println("Der Umfang betr�gt " + umfang);
    	
    	//Automatisierte Auswertung  ------ab hier ist alles korrekt, nichts ver�ndern------
    	return "A2: " + ueberschrift + ";" + pi + "; Durchmesser: " + durchmesser + "; Umfang: " + umfang;
    }
    
} 
