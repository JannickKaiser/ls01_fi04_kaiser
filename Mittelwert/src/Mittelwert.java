import java.util.Scanner;
public class Mittelwert {

	public static double berechneMittelwert(double x1, double x2){
		double m = x1 / x2;
		return m;
		
		
	}
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = 0;
      double anzahl;
      double m;
      
      Scanner scan = new Scanner(System.in);
      System.out.println("Bitte geben Sie die Anzahl ein: ");
      anzahl = scan.nextInt();
      
      
      
      for(int i=1; i<=anzahl; i++) {
    	  System.out.println("Bitte geben sie eine Zahl ein: ");
          x += scan.nextDouble(); 
      }
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      
      m = berechneMittelwert(x, anzahl);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist \n" + m);
  
   }
	
}